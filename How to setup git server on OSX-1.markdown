## How to setup and use git server on OS X

<br>

**Louis Zhu**

**NewPower Co.**

#### Thanks to:

Pro Git: <http://progit.org>

Github's help: <http://help.github.com/mac-set-up-git/>

A useful guide: <http://www.ossxp.com/doc/git/gitolite.html>

#### Steps:

###### Prepare the server

On the server, create a new user named "git".

```
$ sudo adduser --system --shell /bin/bash --group git
$ sudo adduser git ssh

```

Then create a directory for repositories.

```
$ su git
$ mkdir ~/repos
```

###### Prepare workstations

On every workstation, create ssh key-pairs and add the public part to server's "authorized_keys".

```
$ ssh-keygen -t rsa -C <email> -f ~/.ssh/<username>
$ ssh-copy-id -i ~/.ssh/<username>.pub git@<server>
```

Note that there's no "ssh-copy-id" command on OS X by default. You can get one by check out [this](http://ios.svn.dev.yoho.cn/DwarvenTreasury/ssh-copy-id/) svn resource.

Then modify the ssh configuration file to get passwordless access to the server.

```
$ vim ~/.ssh/config
host <servername>
    user git
    hostname <server>
    port 22
    identityfile ~/.ssh/<username>
```

At last, set up the user's information.

```
$ git config --global user.name <username>
$ git config --global user.email <email>
```

###### Add repositories

On the server, initialize a repository.

```
$ cd ~/repos
$ mkdir <newrepositoryname>.git
$ cd <newrepositoryname>.git
$ git --bare init
```

Then on your workstation:

```
$ cd /path/to/the/project
$ git init //optional
$ git add . //optional
$ git commit -m 'initial commit' //optional
$ git remote add origin <servername>:~/repos/<newrepositoryname>.git
$ git push origin master
```

Note that the origin should be <servername> instead of <server>.

Some of the steps above are optional, you should do the steps only if there's no local git repository for your project.

#### Working with git

###### Pull

